/**
 * THE TASK
 * создать начальную стадию игры в догонялки
 *
 * после загрузки на экране появляется красный круг, размером 200 пикселей
 * после любого движения мышкой - он начинает "догонять" курсор.
 * чтобы закончить игру нужно нажать пробел
 *
 * 2 фичи на реализацию:
 * 1 - реализовать саму догонялку. при каждом движении мышки - мониторить координаты курсора и координаты кружка и вычис
 *     вычислять "догнал" кружок мышку или нет
 *
 * 2 - реализовать табло, на котором ведется отсчет времени с того момента, как мышкой пошевелили
 *     если круг "догнал" курсор мышки:
 *      - значение на табло обнуляется
 *      - предыдущий результат нужно сохранять и отображать под отсчетом времени
 *
 * для вычисления сколько прошло с момента шевеления мышкой лучше иметь отдельную функцию
 * для обнуления - тоже.
 */

document.addEventListener("mousemove", () => {});

var myX, myY, xyOn, myMouseX, myMouseY;
xyOn = true;

function getXYPosition(e) {
  myMouseX = e.clientX;
  myMouseY = e.clientY;
  if (document.documentElement.scrollTop > 0) {
    myMouseY = myMouseY + document.documentElement.scrollTop;
  }
  if (xyOn) {
    alert("X is " + myMouseX + "\nY is " + myMouseY);
  }
}
function toggleXY() {
  xyOn = !xyOn;
  document.getElementById("xyLink").blur();
  return false;
}

document.onmouseup = getXYPosition;
